package app;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // вывод в консоль
        System.out.println("Введите возраст");

        // делаем переменную input типа Scanner (то есть выделяется ячейка памяти для типа Scanner, специальный тип данных,
        // запомним его пока просто как тип для сканирования чего-то.  В ячейку выделенную для этого типа мы засовываем конкретное знаение
        // а именно, слово new говорит о том, что создан конкретный сканер и сканировать он будет то, что ввели в консоли System.in
        // то есть в переменной input лежит то, что мы считали из консоли, но тип у этого чего-то не int, а Scanner
        Scanner input = new Scanner(System.in);

        // приводим то, что в переменной input, к целочисленному типу, то есть к int
        int age = input.nextInt();

        // если возраст больше или равно 18, то в переменную message мы записываем "Кредит выдан", если нет, то - "Кредит не выдан"
        String message = (age >= 18) ? "Кредит выдан" : "Кредит не выдан";
        System.out.println(message);


        // 1 часть урока, чтобы раскомментировать - выделить все и нажать комбинацию клавиш ctrl + shift + /
/*

        // вывод в консоль
        System.out.println("Hello world!!!");
        System.out.println("Hello world!!!!!!!!");

        // числовая переменная
        int glass;
        glass = 100;
        System.out.println(glass);
        glass = 400;
        System.out.println(glass);

        // константа - нельзя менять значение далее по ходу программы
        final String STR = "Hello world!!!!!!!!";
        System.out.println(STR);

        // числовая переменная с дробной частью
        double newNumber = 4.45;

        // логическая переменная, два значения: true / false
        boolean isHuman = true;

        isHuman = newNumber >= 0;

        // ищите описание именно этих строчек в файле с домашним заданием, т.к. с написанными комментариями не компилируется
        boolean result = isHuman && (glass == 401);
        boolean result1 = isHuman & (glass == 401);
        boolean result2 = isHuman || (glass == 401);
        boolean result3 = isHuman | (glass == 401);
        System.out.println(result);
        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);

        // символ
        char ch = 'f';
        // не смотря на то, что тип не задан явно, все равно он int из-за введенного значения, которое явно int
        var something  = 5;

        int number = 64;
        // остаток от деления
        number = 64 % 5;
        // тоже самое, что number = number * 5;
        number *= 5;
        System.out.println(number);*/
    }
}
